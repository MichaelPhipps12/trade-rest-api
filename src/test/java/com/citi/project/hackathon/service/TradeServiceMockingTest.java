package com.citi.project.hackathon.service;

import static org.mockito.Mockito.when;

import com.citi.project.hackathon.dao.TradeMongoRepo;
import com.citi.project.hackathon.model.Trade;
import com.citi.project.hackathon.model.TradeState;
import com.citi.project.hackathon.model.TradeType;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

@SpringBootTest
public class TradeServiceMockingTest {

    @Autowired
    private TradeService tradeService;

    @MockBean
    private TradeMongoRepo mockTradeRepo;

    @Test
    public void test_tradeServiceMock_save(){
        Trade testTrade = new Trade();

        testTrade.setDateCreated((java.util.Date) Date(System.currentTimeMillis()));
        testTrade.setRequestedPrice(10);
        testTrade.setStockQuantity(500);
        testTrade.setStockTicker("TSLA");
        testTrade.setTradeState(TradeState.CREATED);
        testTrade.setTradeType(TradeType.BUY);

        // tell mockRepo that employeeService is going to call save()
        // when it does return the testEmployee object
        when(mockTradeRepo.save(testTrade)).thenReturn(testTrade);

        tradeService.save(testTrade);
    } 
    
    private Object Date(long currentTimeMillis) {
        return null;
    }

    @Test
    public void test_findAllMock_sanityCheck() {
        assert(tradeService.findAll().size() == 0);
    }
    
    // a comment
}
