package com.citi.project.hackathon.dao;

import com.citi.project.hackathon.model.Trade;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Component;

@Component
public interface TradeMongoRepo extends MongoRepository<Trade, String> {
    
    //List<Trade> findByAddress(String address);
}