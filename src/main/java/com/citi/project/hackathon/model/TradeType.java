package com.citi.project.hackathon.model;

/**
 * TradeType enum may be: BUY or SELL
 */
public enum TradeType{

    BUY, SELL
}
