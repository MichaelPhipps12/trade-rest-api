package com.citi.project.hackathon.model;

/**
 * TradeState enum may be : CREATED, PROCESSING, FILLED, or REJECTED
 */
public enum TradeState{

    CREATED, PROCESSING, FILLED, REJECTED

}
