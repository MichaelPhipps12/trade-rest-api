package com.citi.project.hackathon.rest;

import java.util.List;

import com.citi.project.hackathon.service.TradeService;
import com.citi.project.hackathon.model.Trade;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin("*")
@RestController
@RequestMapping("/v1/trade")
public class TradeController {

    private static final Logger LOG = LoggerFactory.getLogger(TradeController.class);

    @Autowired
    private TradeService tradeService;

    @RequestMapping(method=RequestMethod.POST)
    public ResponseEntity<Trade> save(@RequestBody Trade trade) {
        LOG.debug("save trade request received");

        return new ResponseEntity<Trade>(tradeService.save(trade),
                                            HttpStatus.CREATED);
    }

    @RequestMapping(method=RequestMethod.GET)
    public List<Trade> findAll() {
        LOG.debug("findAll() request received");
        return tradeService.findAll();
    }

    @RequestMapping(path="/{id}", method=RequestMethod.PUT)
    public ResponseEntity<Trade> update(@PathVariable String id,
                                           @RequestBody Trade trade) {
        LOG.debug("update() request received : " + id);

        return new ResponseEntity<Trade>(tradeService.update(id, trade),
                                            HttpStatus.OK);
    }

}